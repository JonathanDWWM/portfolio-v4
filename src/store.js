import { createStore } from 'vuex'

export default createStore({
  state: {
    currentSection: null,
  },
  mutations: {
    setCurrentSection(state, section) {
      state.currentSection = section;
    },
  },
})
